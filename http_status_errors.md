## Типовые ошибки при обработки запросов

|`"status"`| `"name"` | `"message"` | `"details"`|
|----------|----------|-------------|------------|
|401|UnauthorizedError|Missing or invalid credentials||
|403|ForbiddenError|Forbidden||
|500|InternalServerError|Internal Server Error||
